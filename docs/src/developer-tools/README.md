---
sidebarDepth: 2
---

# Developer Tools

Meltano is primarily offered to end-users as a hosted software-as-a-service (SaaS) solution for busy non-technical business owners looking to gain insights into their business data. However, we are also [an open source project](https://gitlab.com/meltano/meltano), and you have the option to [self-host Meltano](/developer-tools/self-hosted-installation.html).

- [Self-Hosted Installation Guide](/developer-tools/self-hosted-installation.html)
- [Command Line Interface](/developer-tools/command-line-interface.html)
- [Environment Variables](/developer-tools/environment-variables.html)
- [Orchestration](/developer-tools/orchestration.html)
- [Transforms](/developer-tools/transforms.html)
- [Reporting Database Options](/developer-tools/reporting-database-options.html)
- [Role-Based Access Control](/developer-tools/role-based-access-control.html)
- [Open Source Contributor Guide](/developer-tools/contributor-guide.html)
- [Overview of Meltano Architecture](/developer-tools/architecture.html)